package com.app.rx.data.models.homemodel;

import com.app.rx.data.models.base.ResultModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeResultModel extends ResultModel {
    @SerializedName("Doctors")
    @Expose
    private DoctorsResponse doctors;
    @SerializedName("access_token")
    @Expose
    private String accessToken;

    public DoctorsResponse getDoctors() {
        return doctors;
    }

    public void setDoctors(DoctorsResponse doctors) {
        this.doctors = doctors;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
