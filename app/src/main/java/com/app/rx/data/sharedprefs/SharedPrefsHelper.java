package com.app.rx.data.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsHelper {

    public static final String MY_PREFS = "MY_PREFS";

    public static final String EMAIL = "EMAIL";
    public static final String TOKEN = "TOKEN";
    public static final String ID = "ID";
    public static final String Login = "Login";
    public static final String Name = "Name";
    public static final String Email = "Email";
    public static final String Pic = "Pic";

    SharedPreferences mSharedPreferences;

    public SharedPrefsHelper(Context context) {
        mSharedPreferences = context.getSharedPreferences(MY_PREFS,context. MODE_PRIVATE);
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }

    public void puttoken(String token) {
        mSharedPreferences.edit().putString(TOKEN, token).apply();
    }
    public String getToken() {
        return mSharedPreferences.getString(TOKEN, null);
    }

    public void putID(String id) {
        mSharedPreferences.edit().putString(ID, id).apply();
    }
    public String getID() {
        return mSharedPreferences.getString(ID, "0");
    }

    public void setlogin(String login) {
        mSharedPreferences.edit().putString(Login, login).apply();
    }
    public String getLogin() {
        return mSharedPreferences.getString(Login, "0");
    }

    //name
    public void setname(String name) {
        mSharedPreferences.edit().putString(Name, name).apply();
    }
    public String getname() {
        return mSharedPreferences.getString(Name, "0");
    }
    //email
    public void setemail(String email) {
        mSharedPreferences.edit().putString(Email, email).apply();
    }
    public String getemail() {
        return mSharedPreferences.getString(Email, "0");
    }
    //pic
    public void setPic(String pic) {
        mSharedPreferences.edit().putString(Pic, pic).apply();
    }
    public String getpic() {
        return mSharedPreferences.getString(Pic, "0");
    }

}