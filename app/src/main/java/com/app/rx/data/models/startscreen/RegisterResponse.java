package com.app.rx.data.models.startscreen;

import com.app.rx.data.models.base.BaseModel;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse extends BaseModel {

    @SerializedName("result")
    private RegisterResultModel result;

    public RegisterResultModel getResult() {
        return result;
    }

    public void setResult(RegisterResultModel result) {
        this.result = result;
    }
}
