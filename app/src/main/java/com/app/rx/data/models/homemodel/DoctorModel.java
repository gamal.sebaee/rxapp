package com.app.rx.data.models.homemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorModel {
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("doctor_id")
    @Expose
    private Integer doctorId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address_en")
    @Expose
    private String addressEn;
    @SerializedName("address_ar")
    @Expose
    private String addressAr;
    @SerializedName("days_ar")
    @Expose
    private String daysAr;
    @SerializedName("days_en")
    @Expose
    private String daysEn;
    @SerializedName("times_ar")
    @Expose
    private String timesAr;
    @SerializedName("times_en")
    @Expose
    private String timesEn;
    @SerializedName("description_ar")
    @Expose
    private String descriptionAr;
    @SerializedName("description_en")
    @Expose
    private String descriptionEn;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("photo")
    @Expose
    private String photo;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressEn() {
        return addressEn;
    }

    public void setAddressEn(String addressEn) {
        this.addressEn = addressEn;
    }

    public String getAddressAr() {
        return addressAr;
    }

    public void setAddressAr(String addressAr) {
        this.addressAr = addressAr;
    }

    public String getDaysAr() {
        return daysAr;
    }

    public void setDaysAr(String daysAr) {
        this.daysAr = daysAr;
    }

    public String getDaysEn() {
        return daysEn;
    }

    public void setDaysEn(String daysEn) {
        this.daysEn = daysEn;
    }

    public String getTimesAr() {
        return timesAr;
    }

    public void setTimesAr(String timesAr) {
        this.timesAr = timesAr;
    }

    public String getTimesEn() {
        return timesEn;
    }

    public void setTimesEn(String timesEn) {
        this.timesEn = timesEn;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
