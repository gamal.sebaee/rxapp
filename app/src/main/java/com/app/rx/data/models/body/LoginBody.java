package com.app.rx.data.models.body;

import com.google.gson.annotations.SerializedName;

public class LoginBody {
    @SerializedName("email")
    private String userEmail;
    @SerializedName("password")
    private String userPassword;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
