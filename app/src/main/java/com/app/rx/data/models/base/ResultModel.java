package com.app.rx.data.models.base;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultModel {
    @SerializedName("errors")
    private List<String> allErrors=null;

    public List<String> getAllErrors() {
        return allErrors;
    }

    public void setAllErrors(List<String> allErrors) {
        this.allErrors = allErrors;
    }
}
