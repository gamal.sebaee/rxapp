package com.app.rx.data.models.homemodel;

import com.app.rx.data.models.base.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeResponse extends BaseModel {
    @SerializedName("result")
    @Expose
    private HomeResultModel result;


    public HomeResultModel getResult() {
        return result;
    }

    public void setResult(HomeResultModel result) {
        this.result = result;
    }

}
