package com.app.rx.data.sharedprefs;

public class DataManager {

    SharedPrefsHelper mSharedPrefsHelper;

    public DataManager(SharedPrefsHelper sharedPrefsHelper) {
        mSharedPrefsHelper = sharedPrefsHelper;
    }

    public void clear() {
        mSharedPrefsHelper.clear();
    }

    public void saveToken(String token) {
        mSharedPrefsHelper.puttoken(token);
    }

    public String getToken() {
        return mSharedPrefsHelper.getToken();
    }

    public void saveId(String id) {
        mSharedPrefsHelper.putID(id);
    }

    public String getId() {
        return mSharedPrefsHelper.getID();
    }

    public void saveLogin(String login) {
        mSharedPrefsHelper.setlogin(login);
    }

    public String getSaveLogin() {
        return mSharedPrefsHelper.getLogin();
    }

    //name
    public void savename(String name) {
        mSharedPrefsHelper.setname(name);
    }

    public String getsaveName() {
        return mSharedPrefsHelper.getname();
    }

    //email
    public void saveemail(String email) {
        mSharedPrefsHelper.setemail(email);
    }

    public String getSaveemail() {
        return mSharedPrefsHelper.getemail();
    }

    //pic
    public void savepic(String pic) {
        mSharedPrefsHelper.setPic(pic);
    }

    public String getSavepic() {
        return mSharedPrefsHelper.getpic();
    }
}