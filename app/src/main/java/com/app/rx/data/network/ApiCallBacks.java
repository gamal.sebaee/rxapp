package com.app.rx.data.network;

public interface ApiCallBacks {
    void successResponse(Object response);
    void failResponse(Object error);
}
