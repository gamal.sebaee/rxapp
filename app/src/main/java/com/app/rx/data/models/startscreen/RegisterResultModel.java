package com.app.rx.data.models.startscreen;

import com.app.rx.data.models.base.ResultModel;
import com.google.gson.annotations.SerializedName;

public class RegisterResultModel extends ResultModel {

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("access_token")
    private String access_token;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
