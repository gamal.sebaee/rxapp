package com.app.rx.data.network;

/**
 * Created by ahmed on 3/7/2018.
 */

import com.app.rx.R;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {
    //Web service URL
    private static final String baseUrl="https://rx.mpgline.com/";
    public static final String image_link=baseUrl+"images/";
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(1000, TimeUnit.SECONDS).hostnameVerifier((s, sslSession) -> true)
            .readTimeout(7000, TimeUnit.SECONDS).writeTimeout(7000,TimeUnit.SECONDS).build();

    private static Retrofit.Builder builder =
            new Retrofit.Builder().client(okHttpClient)
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create());
    static ApiInterface createService() {

        Retrofit retrofit = builder.build();
        return retrofit.create( ApiInterface.class);
    }

}