package com.app.rx.data.models.startscreen;

import com.app.rx.data.models.base.BaseModel;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends BaseModel {
    @SerializedName("result")
    private LoginResultModel result;

    public LoginResultModel getResult() {
        return result;
    }

    public void setResult(LoginResultModel result) {
        this.result = result;
    }
}
