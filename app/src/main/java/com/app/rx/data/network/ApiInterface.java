package com.app.rx.data.network;

import com.app.rx.data.models.homemodel.HomeResponse;
import com.app.rx.data.models.startscreen.RegisterResponse;
import com.app.rx.data.models.body.LoginBody;
import com.app.rx.data.models.startscreen.LoginResponse;
import com.app.rx.data.models.body.RegisterBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ahmed on 3/7/2018.
 */

public interface ApiInterface {
    @Headers("Content-Type:application/json")
    @POST("api/login")
    Call<LoginResponse> login(@Body LoginBody loginBody);

    @Headers("Content-Type:application/json")
    @POST("api/register")
    Call<RegisterResponse> registerNewUser(@Body RegisterBody registerBody);

    @Headers("Content-Type:application/json")
    @GET("api/doctors")
    Call<HomeResponse> getAllDoctors(@Query("access_token") String access_token,@Query("lat") String lat,@Query("lng") String lng);

}
