package com.app.rx.data.network;

import com.app.rx.data.models.base.ResultModel;
import com.app.rx.data.models.body.LoginBody;
import com.app.rx.data.models.homemodel.HomeResponse;
import com.app.rx.data.models.startscreen.LoginResponse;
import com.app.rx.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiManger {
    private ApiInterface apiInterface;
    private String statusDone = "done";


    public ApiManger() {
        apiInterface = RetrofitBuilder.createService();

    }

    public void login(String userEmail, String userPassword, ApiCallBacks apiCallBacks) {
        LoginBody loginBody = new LoginBody();
        loginBody.setUserEmail(userEmail);
        loginBody.setUserPassword(userPassword);
        Call<LoginResponse> responseData = apiInterface.login(loginBody);
        responseData.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NotNull Call<LoginResponse> call, @NotNull Response<LoginResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatue() != null && response.body().getStatue().trim().toLowerCase().equals(statusDone)) {
                        apiCallBacks.successResponse(response.body().getResult());
                    } else {
                        vaildateResponse(response.body().getResult(), apiCallBacks);
                    }

                } else if (response.errorBody() != null) {
                    try {
                        apiCallBacks.failResponse(response.errorBody().string());
                    } catch (IOException e) {
                        apiCallBacks.failResponse("fail to load data ");
                    }
                }

            }

            @Override
            public void onFailure(@NotNull Call<LoginResponse> call, @NotNull Throwable t) {
                apiCallBacks.failResponse(t.getMessage());
            }
        });

    }
 public void getAllDoctors(String userLat, String userLng, ApiCallBacks apiCallBacks) {

        Call<HomeResponse> responseData = apiInterface.getAllDoctors(Constants.USER_API_TOKEN,
                userLat,userLng);
        responseData.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(@NotNull Call<HomeResponse> call, @NotNull Response<HomeResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatue() != null
                            && response.body().getStatue().trim().toLowerCase().equals(statusDone)) {
                        apiCallBacks.successResponse(response.body().getResult());
                    } else {
                        vaildateResponse(response.body().getResult(), apiCallBacks);
                    }

                } else if (response.errorBody() != null) {
                    try {
                        apiCallBacks.failResponse(response.errorBody().string());
                    } catch (IOException e) {
                        apiCallBacks.failResponse("fail to load data ");
                    }
                }

            }

            @Override
            public void onFailure(@NotNull Call<HomeResponse> call, @NotNull Throwable t) {
                apiCallBacks.failResponse(t.getMessage());
            }
        });

    }

    private void vaildateResponse(ResultModel baseModel, ApiCallBacks apiCallBacks) {
        if (baseModel.getAllErrors() != null && baseModel.getAllErrors().size() > 0) {
            apiCallBacks.failResponse(baseModel.getAllErrors().get(0));
        }
    }

}


