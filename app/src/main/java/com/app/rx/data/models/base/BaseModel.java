package com.app.rx.data.models.base;

import com.google.gson.annotations.SerializedName;


public class BaseModel {

    @SerializedName("statue")
    private String statue;

    public String getStatue() {
        return statue;
    }

    public void setStatue(String statue) {
        this.statue = statue;
    }
}
