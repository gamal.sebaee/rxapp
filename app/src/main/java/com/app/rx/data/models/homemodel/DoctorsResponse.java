package com.app.rx.data.models.homemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorsResponse {
    @SerializedName("category")
    @Expose
    private List<CategoryModel> category = null;
    @SerializedName("doctor")
    @Expose
    private List<DoctorModel> doctor = null;
    @SerializedName("nearbyDoctor")
    @Expose
    private List<List<Object>> nearbyDoctor = null;

    public List<CategoryModel> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryModel> category) {
        this.category = category;
    }

    public List<DoctorModel> getDoctor() {
        return doctor;
    }

    public void setDoctor(List<DoctorModel> doctor) {
        this.doctor = doctor;
    }

    public List<List<Object>> getNearbyDoctor() {
        return nearbyDoctor;
    }

    public void setNearbyDoctor(List<List<Object>> nearbyDoctor) {
        this.nearbyDoctor = nearbyDoctor;
    }
}
