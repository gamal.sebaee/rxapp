package com.app.rx.ui.activities.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.rx.R;
import com.app.rx.ui.Base.BaseActivity;
import com.app.rx.ui.activities.home.HomeActivity;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.gson.Gson;

public class LogingActivity extends BaseActivity implements LoginMVpView, View.OnClickListener {
    ImageView log_in;
    EditText et_userName;
    EditText et_userPasword;
    private LoginMVpPresenter loginMVpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loging);
        loginMVpPresenter = new LoginMVpPresenter(apiManger, this);
        initViews();
    }

    @Override
    public void initViews() {
        log_in = findViewById(R.id.login);
        et_userName = findViewById(R.id.et_userName);
        et_userPasword = findViewById(R.id.et_userPasword);
        log_in.setOnClickListener(this);

        et_userName.setText("gamal.sebaee1@gmail.com");
        et_userPasword.setText("123456");
    }

    @Override
    public void successResponse(Object sucessObj) {
        openHomeActivitiy();
       // Log.d("loginData", "" + new Gson().toJson(sucessObj));
        //Toast.makeText(this, "" + new Gson().toJson(sucessObj), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void failResponse(Object failObj) {
        Toast.makeText(this, "" + failObj, Toast.LENGTH_SHORT).show();

    }

    private void openHomeActivitiy() {

        /* Create an Intent that will start the Menu-Activity. */
        Intent mainIntent = new Intent(LogingActivity.this, HomeActivity.class);
        startActivity(mainIntent);
        this.finish();
        Animatoo.animateZoom(LogingActivity.this); //fire the slide left animation
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login) {
            /*loginMVpPresenter.confirmLogin(et_userName.getText().toString(),
                    et_userPasword.getText().toString());*/
            openHomeActivitiy();
        }
    }
}
