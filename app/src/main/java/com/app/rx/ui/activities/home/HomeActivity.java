package com.app.rx.ui.activities.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.rx.R;
import com.app.rx.ui.activities.SplashScreen.Splash;
import com.app.rx.ui.activities.slider.SliderActivity;
import com.app.rx.ui.fragments.doctormap.DoctorMap;
import com.app.rx.ui.fragments.doctors.Doctors;
import com.app.rx.ui.fragments.homefragment.HomeFragment;
import com.app.rx.ui.fragments.navigation.NavigationDrawer;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView menu, close, map, logo, home, doctor, searchdoctor, drugs, profile;
    CardView card_container;
    FrameLayout fram, rootview;
    TextView title;
    RelativeLayout rel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setInitial();
        home.setBackgroundResource(R.drawable.homeactive);
        doctor.setBackgroundResource(R.drawable.doctorunactive);
        searchdoctor.setBackgroundResource(R.drawable.searchunactive);
        drugs.setBackgroundResource(R.drawable.drugunactive);
        profile.setBackgroundResource(R.drawable.profileunactive);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();

    }

    public void setInitial() {
        menu = findViewById(R.id.menu);

        searchdoctor = findViewById(R.id.searchdoctor);
        drugs = findViewById(R.id.drugs);
        profile = findViewById(R.id.profile);

        home = findViewById(R.id.home);
        doctor = findViewById(R.id.doctor);
        close = findViewById(R.id.close);
        logo = findViewById(R.id.logo);
        rel = findViewById(R.id.rel);
        card_container = findViewById(R.id.card_container);
        fram = findViewById(R.id.fram);
        rootview = findViewById(R.id.rootview);
        map = findViewById(R.id.map);
        title = findViewById(R.id.title);
        menu.setOnClickListener(this);
        close.setOnClickListener(this);
        home.setOnClickListener(this);
        doctor.setOnClickListener(this);
        searchdoctor.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                map.setVisibility(View.GONE);
                logo.setVisibility(View.VISIBLE);
                title.setText("Menu");
                title.setTextColor(getResources().getColor(R.color.colorAccent));

                rootview.setVisibility(View.GONE);
                menu.setVisibility(View.GONE);
                close.setVisibility(View.VISIBLE);
                card_container.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
                fram.setBackgroundColor(getResources().getColor(R.color.fram));
                rel.setBackgroundColor(getResources().getColor(R.color.fram));
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new NavigationDrawer()).commit();
                break;
            case R.id.close:
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(HomeActivity.this, HomeActivity.class);
                startActivity(mainIntent);
                finish();
                Animatoo.animateSplit(HomeActivity.this); //fire the slide left animation
                break;
            case R.id.home:
                home.setBackgroundResource(R.drawable.homeactive);
                doctor.setBackgroundResource(R.drawable.doctorunactive);
                searchdoctor.setBackgroundResource(R.drawable.searchunactive);
                drugs.setBackgroundResource(R.drawable.drugunactive);
                profile.setBackgroundResource(R.drawable.profileunactive);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.doctor:
                title.setText("Doctors");
                home.setBackgroundResource(R.drawable.homenotactove);
                doctor.setBackgroundResource(R.drawable.doctoractive);
                searchdoctor.setBackgroundResource(R.drawable.searchunactive);
                drugs.setBackgroundResource(R.drawable.drugunactive);
                profile.setBackgroundResource(R.drawable.profileunactive);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new Doctors()).commit();
                break;
                case R.id.searchdoctor:
                title.setText("Doctors");
                home.setBackgroundResource(R.drawable.homenotactove);
                doctor.setBackgroundResource(R.drawable.doctorunactive);
                searchdoctor.setBackgroundResource(R.drawable.searchactive);
                drugs.setBackgroundResource(R.drawable.drugunactive);
                profile.setBackgroundResource(R.drawable.profileunactive);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new DoctorMap()).commit();
                break;
        }
    }
}
