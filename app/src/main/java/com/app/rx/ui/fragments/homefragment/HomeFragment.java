package com.app.rx.ui.fragments.homefragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.app.rx.R;
import com.app.rx.data.models.homemodel.PublarList;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements HomeMVPView {
    RecyclerView recommend_doctor, publur_doctor, publur_drugs;
    PubularAdapter mAdapter;
    DoctorAdapter doctorAdapter;
    DrugsAdapter drugsAdapter;
    ArrayList<PublarList> publarListArrayList;
    View view;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initView();
        getPubular(publarListArrayList);
        getdoctors(publarListArrayList);
        getDrugs(publarListArrayList);

        return view;
    }

    @Override
    public void initView() {
        publarListArrayList = new ArrayList<>();
        recommend_doctor = view.findViewById(R.id.recommend_doctor);
        publur_drugs = view.findViewById(R.id.publur_drugs);
        publur_doctor = view.findViewById(R.id.publur_doctor);
    }

    @Override
    public void progressDialogeShow() {

    }

    @Override
    public void progressDialogedismiss() {

    }

    @Override
    public void getPubular(ArrayList<PublarList> publarListArrayList) {

        mAdapter = new PubularAdapter(publarListArrayList, getActivity());
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recommend_doctor.setLayoutManager(layoutManager);
        recommend_doctor.setAdapter(mAdapter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.anim_recycle);
        recommend_doctor.setLayoutAnimation(controller);
        recommend_doctor.getAdapter().notifyDataSetChanged();
        recommend_doctor.scheduleLayoutAnimation();
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void getdoctors(ArrayList<PublarList> publarListArrayList) {
        doctorAdapter = new DoctorAdapter(publarListArrayList, getActivity());
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        publur_doctor.setLayoutManager(layoutManager);
        publur_doctor.setAdapter(doctorAdapter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.anim_recycle);
        publur_doctor.setLayoutAnimation(controller);
        publur_doctor.getAdapter().notifyDataSetChanged();
        publur_doctor.scheduleLayoutAnimation();
        doctorAdapter.notifyDataSetChanged();
    }

    @Override
    public void getDrugs(ArrayList<PublarList> publarListArrayList) {
        drugsAdapter = new DrugsAdapter(publarListArrayList, getActivity());
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        publur_drugs.setLayoutManager(layoutManager);
        publur_drugs.setAdapter(drugsAdapter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.anim_recycle);
        publur_drugs.setLayoutAnimation(controller);
        publur_drugs.getAdapter().notifyDataSetChanged();
        publur_drugs.scheduleLayoutAnimation();
        drugsAdapter.notifyDataSetChanged();
    }
}
