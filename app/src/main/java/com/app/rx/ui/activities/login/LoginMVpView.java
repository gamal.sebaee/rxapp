package com.app.rx.ui.activities.login;

import com.app.rx.ui.Base.MvpView;

public interface LoginMVpView extends MvpView {
    void initViews();
    void successResponse(Object sucessObj);
    void failResponse(Object failObj);

}
