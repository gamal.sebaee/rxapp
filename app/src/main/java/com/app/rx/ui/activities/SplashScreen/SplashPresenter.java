package com.app.rx.ui.activities.SplashScreen;

import android.os.Handler;


import com.app.rx.data.sharedprefs.DataManager;
import com.app.rx.ui.Base.BasePresenter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by foush on 09/02/18.
 */

public class SplashPresenter <V extends SplashView>extends BasePresenter<V> implements SplashMvpPresenter<V> {
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    public SplashPresenter(DataManager dataManager) {
        super(dataManager);
    }
    @Override
    public void DecideNextActivity() {

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                if(getDataManager().getSaveLogin().equals("1")){
                    getMvpView().openMainActivity();
                }else {//if not logged in {open LoggInActivity}
                    getMvpView().openLoginActivity();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);

    }



}
