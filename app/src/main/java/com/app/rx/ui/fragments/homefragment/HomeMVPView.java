package com.app.rx.ui.fragments.homefragment;

import com.app.rx.data.models.homemodel.PublarList;
import com.app.rx.ui.Base.MvpView;

import java.util.ArrayList;

public interface HomeMVPView extends MvpView {
    void initView();
    void progressDialogeShow();
    void progressDialogedismiss();
    void getPubular(ArrayList<PublarList> publarListArrayList );
    void getdoctors(ArrayList<PublarList> publarListArrayList );
    void getDrugs(ArrayList<PublarList> publarListArrayList );
}
