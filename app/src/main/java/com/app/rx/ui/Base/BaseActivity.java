package com.app.rx.ui.Base;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.rx.data.network.ApiManger;

/**
 * Created by foush on 09/02/18.
 */

public class BaseActivity extends AppCompatActivity implements MvpView {

    public ApiManger apiManger;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiManger=new ApiManger();
    }
}
