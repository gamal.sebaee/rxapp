package com.app.rx.ui.activities.slider;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.app.rx.R;
import com.app.rx.ui.activities.login.LogingActivity;
import com.app.rx.ui.activities.register.Registration;
import com.app.rx.ui.activities.transition.CustPagerTransformer;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.eftimoff.viewpagertransformers.DepthPageTransformer;
import com.eftimoff.viewpagertransformers.RotateUpTransformer;
import com.rd.PageIndicatorView;

public class SliderActivity extends AppCompatActivity implements SliderMVPView, View.OnClickListener {
    private ViewPager viewPager;
    private PageIndicatorView circleIndicator;
    private Adaptring adaptring;
    ImageView login,sing_up;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);
        initViews();
    }

    @Override
    public void initViews() {
        viewPager = findViewById(R.id.viewPager);
        login = findViewById(R.id.login);
        sing_up = findViewById(R.id.sing_up);
        adaptring = new Adaptring(this);
        viewPager.setAdapter(adaptring);
        viewPager.setPageTransformer(true, new CustPagerTransformer(this));
        circleIndicator = findViewById(R.id.pageIndicatorView);
        circleIndicator.setViewPager(viewPager);
        login.setOnClickListener(this);
        sing_up.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login:
                startActivity(new Intent(SliderActivity.this, LogingActivity.class));
                Animatoo.animateZoom(SliderActivity.this);  //fire the zoom animation
                break;
                case R.id.sing_up:
                startActivity(new Intent(SliderActivity.this, Registration.class));
                Animatoo.animateZoom(SliderActivity.this);  //fire the zoom animation
                break;
        }
    }
}
