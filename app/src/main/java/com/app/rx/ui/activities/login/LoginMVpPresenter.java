package com.app.rx.ui.activities.login;

import com.app.rx.data.network.ApiCallBacks;
import com.app.rx.data.network.ApiManger;
import com.app.rx.ui.Base.MvpView;

public class LoginMVpPresenter {
    private LoginMVpView loginMVpView;
    private ApiManger apiManger;

    public LoginMVpPresenter(ApiManger apiManger, LoginMVpView loginMVpView) {
        this.loginMVpView = loginMVpView;
        this.apiManger = apiManger;
    }

    void confirmLogin(String userEmail, String userPassword) {

        apiManger.login(userEmail, userPassword, new ApiCallBacks() {
            @Override
            public void successResponse(Object response) {
                loginMVpView.successResponse(response);
            }

            @Override
            public void failResponse(Object error) {
                loginMVpView.failResponse(error);
            }
        });

    }

}
