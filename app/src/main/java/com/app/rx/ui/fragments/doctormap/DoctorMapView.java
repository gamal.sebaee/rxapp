package com.app.rx.ui.fragments.doctormap;

import com.app.rx.ui.Base.MvpView;

public interface DoctorMapView extends MvpView {
    void initView();
    void progressDialogeShow();
    void progressDialogedismiss();
    void initMap();
}
