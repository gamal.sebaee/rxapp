package com.app.rx.ui.activities;

import android.app.Application;

import com.app.rx.data.sharedprefs.DataManager;
import com.app.rx.data.sharedprefs.SharedPrefsHelper;


/**
 * Created by foush on 09/02/18.
 */

public class otgMvp extends Application {
    SharedPrefsHelper mSharedPrefsHelper;
    DataManager mDataManger;

    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPrefsHelper=new SharedPrefsHelper(getApplicationContext());
        mDataManger=new DataManager(mSharedPrefsHelper);
    }
    public DataManager getmDataManger(){return mDataManger;}
}
