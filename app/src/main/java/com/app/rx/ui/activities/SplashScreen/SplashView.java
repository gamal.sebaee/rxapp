package com.app.rx.ui.activities.SplashScreen;


import com.app.rx.ui.Base.MvpView;

import java.util.ArrayList;

public interface SplashView extends MvpView {
    void openMainActivity();
    void openLoginActivity();

}
