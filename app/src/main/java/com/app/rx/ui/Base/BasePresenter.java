package com.app.rx.ui.Base;


import com.app.rx.data.sharedprefs.DataManager;

/**
 * Created by foush on 09/02/18.
 */

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {
    DataManager mDataManager;
    private V mMvpView;

    public BasePresenter(DataManager dataManager){
        mDataManager=dataManager;
    }
    @Override
    public void onAttach(V MvpView) {
        mMvpView=MvpView;
    }
    public V getMvpView(){
        return mMvpView;
    }
    public DataManager getDataManager(){
        return mDataManager;
    }

}
