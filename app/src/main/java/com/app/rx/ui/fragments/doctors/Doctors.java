package com.app.rx.ui.fragments.doctors;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.app.rx.R;
import com.app.rx.data.models.homemodel.PublarList;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Doctors extends Fragment implements DoctorViews {
    CategoryAdapter categoryAdapter;
    CategoryResultAdapter categoryResultAdapter;
    NearAdapter nearAdapter;
    RecyclerView category,cateoryResult,nearby_doctors;
    View view;
    ArrayList<PublarList> publarListArrayList;
    public Doctors() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_doctors, container, false);
        initView();
        getCategory(publarListArrayList);
        getCategoryResults(publarListArrayList);
        getNearDoctor(publarListArrayList);

        return view;
    }

    @Override
    public void initView() {
        publarListArrayList=new ArrayList<>();
        category=view.findViewById(R.id.category);
        cateoryResult=view.findViewById(R.id.cateoryResult);
        nearby_doctors=view.findViewById(R.id.nearby_doctors);
    }

    @Override
    public void progressDialogeShow() {

    }

    @Override
    public void progressDialogedismiss() {

    }

    @Override
    public void getCategory(ArrayList<PublarList> publarListArrayList) {
        categoryAdapter = new CategoryAdapter(publarListArrayList, getActivity());
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        category.setLayoutManager(layoutManager);
        category.setAdapter(categoryAdapter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.anim_recycle);
        category.setLayoutAnimation(controller);
        category.getAdapter().notifyDataSetChanged();
        category.scheduleLayoutAnimation();
        categoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void getCategoryResults(ArrayList<PublarList> publarListArrayList) {
        categoryResultAdapter = new CategoryResultAdapter(publarListArrayList, getActivity());
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        cateoryResult.setLayoutManager(layoutManager);
        cateoryResult.setAdapter(categoryResultAdapter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.anim_recycle);
        cateoryResult.setLayoutAnimation(controller);
        cateoryResult.getAdapter().notifyDataSetChanged();
        cateoryResult.scheduleLayoutAnimation();
        categoryResultAdapter.notifyDataSetChanged();
    }

    @Override
    public void getNearDoctor(ArrayList<PublarList> publarListArrayList) {
        nearAdapter = new NearAdapter(publarListArrayList, getActivity());

        nearby_doctors.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        nearby_doctors.setAdapter(nearAdapter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.anim_recycle);
        nearby_doctors.setLayoutAnimation(controller);
        nearby_doctors.getAdapter().notifyDataSetChanged();
        nearby_doctors.scheduleLayoutAnimation();
        nearAdapter.notifyDataSetChanged();
    }
}
