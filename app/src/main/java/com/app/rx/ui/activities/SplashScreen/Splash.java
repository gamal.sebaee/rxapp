package com.app.rx.ui.activities.SplashScreen;

import android.content.Intent;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.app.rx.R;
import com.app.rx.data.sharedprefs.DataManager;
import com.app.rx.ui.activities.otgMvp;
import com.app.rx.ui.activities.slider.SliderActivity;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;


public class Splash extends AppCompatActivity implements SplashView{
    /** Duration of wait **/

    DataManager mDataManager;
    SplashPresenter mSplashPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mDataManager=((otgMvp)getApplication()).getmDataManger();
        mSplashPresenter=new SplashPresenter(mDataManager);
        mSplashPresenter.onAttach(this);
        mSplashPresenter.DecideNextActivity();
    }

    @Override
    public void openMainActivity() {

//        /* Create an Intent that will start the Menu-Activity. */
//        Intent mainIntent = new Intent(Splash.this, HomePage.class);
//        Splash.this.startActivity(mainIntent);
//        Splash.this.finish();
//        Animatoo.animateSwipeLeft(Splash.this); //fire the slide left animation

    }
    @Override
    public void openLoginActivity() {
        /* Create an Intent that will start the Menu-Activity. */
        Intent mainIntent = new Intent(Splash.this, SliderActivity.class);
        Splash.this.startActivity(mainIntent);
        Splash.this.finish();
        Animatoo.animateZoom(Splash.this); //fire the slide left animation

    }


}
