package com.app.rx.ui.fragments.homefragment;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.app.rx.R;
import com.app.rx.data.models.homemodel.PublarList;

import java.util.List;

/**
 * Created by Ahmed on 7/10/2018.
 */

class PubularAdapter extends RecyclerView.Adapter<PubularAdapter.MyViewHolder>{
    private List<PublarList> moviesList;
    Context context;
    OnItemClickListener onItemClickListener;
    public PubularAdapter(List<PublarList> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }
    public interface OnItemClickListener {
        void onclick(int position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recommended_doctor_item, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

//        holder.image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (onItemClickListener != null) {
//                    onItemClickListener.onclick(position);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public void setOnClickListener(OnItemClickListener onClickListener) {
        this.onItemClickListener = onClickListener;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView    name;
        public MyViewHolder(View view) {
            super(view);
        //     image = itemView.findViewById(R.id.image);


        }
    }


}
