package com.app.rx.ui.activities.slider;

import com.app.rx.ui.Base.MvpView;

public interface SliderMVPView extends MvpView {
   void initViews();
}
