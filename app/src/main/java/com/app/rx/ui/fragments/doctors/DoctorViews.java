package com.app.rx.ui.fragments.doctors;

import com.app.rx.data.models.homemodel.PublarList;
import com.app.rx.ui.Base.MvpView;

import java.util.ArrayList;

public interface DoctorViews extends MvpView {
    void initView();
    void progressDialogeShow();
    void progressDialogedismiss();
    void getCategory(ArrayList<PublarList> publarListArrayList );
    void getCategoryResults(ArrayList<PublarList> publarListArrayList );
    void getNearDoctor(ArrayList<PublarList> publarListArrayList );
}
