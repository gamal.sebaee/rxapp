package com.app.rx.ui.fragments.profile;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.rx.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFrament extends Fragment {


    public ProfileFrament() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_frament, container, false);
    }

}
