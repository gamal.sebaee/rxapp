package com.app.rx.utils;

import android.content.Context;

import com.app.rx.R;
import com.kaopiz.kprogresshud.KProgressHUD;


public class ProgressLoading {
    private KProgressHUD show;
    Context context;

    public ProgressLoading(Context context) {
        this.context = context;
    }

    public void showLoading() {
        show = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setBackgroundColor(R.color.colorAccent)
                .setDimAmount(0.5f)
                .show();
    }
    public void showLoadingWithProgress() {

        show = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
                .setLabel("انتظر قليلا")
                .setMaxProgress(100)
                .show();
    }

    public void cancelLoading() {
        show.dismiss();
    }
    public void updateProgress(int progress) {
        show.setProgress(progress);
    }


}
